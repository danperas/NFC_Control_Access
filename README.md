# NFC Control Access System
NFC control access system for an association at the university. Centralized database for user information, bookings, etc. A Raspberry Pi runs all the code, and it is connected to two NFC readers by two independent SPI bus. Concurrence is mandatory. Threads are used, as well as the IRQ (Interruption) pins for very low CPU usage (non-blocking waiting).

## Instructions
##### Run the packages installation with the following command:
- pip3 install -r requirements.txt

##### Move the *'web'* folder to your app path:  */var/www/web*
##### Move the *'data'* folder to your data path:  */var/www/data*
