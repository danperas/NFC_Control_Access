#!/usr/bin/python3

import time
import datetime
import threading
import queue

import sqlite3
# import RPi.GPIO as GPIO

import reader

ID_DOOR_MAIN = 1
ID_DOOR_STUDY = 2
LEVEL_MEMBER = 1
LEVEL_COUNCIL = 2

OPEN = 1
DENNIE = 2
ADD = 3

DDBB = "/var/www/data/database.db"


def main():
    GPIO.cleanup()
    db = db_driver.Database([DDBB, ])
    try:
        db.connect()
    except sqlite3.Error:
        return None
    else:
        reader_main = reader.Reader()
        reader_main.init_gpio()
        flashing = threading.Event()
        flashing.clear()
        led_mode = queue.Queue()
        t = threading.Thread(target=reader.led_flash, args=(ID_DOOR_MAIN, led_mode, flashing,))
        flashing.set()
        t.start()
        led_mode.put(ADD)
        date_past = datetime.datetime.now()
        date_future = date_past + datetime.timedelta(seconds=30)
        while date_past <= datetime.datetime.now() <= date_future:
            try:
                card = reader_main.read_tag()
                if card is not None:
                    card = int(card)
                    try:
                        db.check_user(card)
                    except ValueError:
                        led_mode.put(OPEN)
                        time.sleep(1)
                        db.disconnect()
                        flashing.clear()
                        t.join()
                        GPIO.cleanup()
                        return card
                    else:
                        led_mode.put(DENNIE)
                        time.sleep(2)
                        db.disconnect()
                        flashing.clear()
                        t.join()
                        GPIO.cleanup()
                        return False
            except KeyboardInterrupt:
                break
        db.disconnect()
        flashing.clear()
        t.join()
        GPIO.cleanup()
        return None


if __name__ == '__main__':
    uid = main()
    print(uid)
