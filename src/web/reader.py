import time
import queue
import threading

# import RPi.GPIO as GPIO
# import pirc522

# ID_DOOR_MAIN  = 1
# ID_DOOR_STUDY = 2
# LED_1_RED     = 3
# LED_1_GREEN   = 5
# LED_1_BLUE    = 7
# LED_2_RED     = 11
# LED_2_GREEN   = 13
# LED_2_BLUE    = 15
# SPI_1_IRQ     = 16
# SPI_2_IRQ     = 18
# SPI_1_RESET   = 22
# SPI_1_CE      = 24
# PIN_OPEN_MAIN = 31
# PIN_OPEN_STUDY = 32
# SPI_2_CE      = 36
# SPI_2_RESET   = 37

# READ = 0
# OPEN = 1
# DENNIE = 2
# ADD = 3


class Reader:

    def __init__(self):
        """
        Set up the MFRC522 object with the SPI pins specified.
        """
        self.sanction = 0
        self.rdr = pirc522.RFID(bus=0, pin_ce=24, pin_rst=22, pin_irq=16)
        self.door = 31
        self.red = 3
        self.green = 5
        self.blue = 7

    def read(self):
        """
        Return an UID if a tag is recognized and near the interrogator.
        :return: UID - String with the Unique Identification tag number.
        """
        wait = self.rdr.wait_for_tag
        rqt = self.rdr.request
        ant = self.rdr.anticoll

        # Wait for a tag within the interrogator range.
        wait()
        (error, tag_type) = rqt()
        if not error:
            (error, uid) = ant()
            if not error:
                return "".join(uid)
            else:
                return None
        else:
            raise None

    def init(self):
        """
        Initializes the RGB LED and the Door Activator.
        """
        r = self.red
        g = self.green
        b = self.blue
        d = self.door
        m = GPIO.BOARD
        o = GPIO.OUT
        s = GPIO.setup

        # Set the BOARD mode and the initial values.
        GPIO.setmode(m)
        s(d, o, initial=1)  # Active Low pin.
        s(r, o, initial=0)
        s(g, o, initial=0)
        s(b, o, initial=0)

    def open(self):
        d = self.door
        o = GPIO.output
        t = time.sleep

        # Triggers the door pin activator and wait a time interval.
        o(d, 0)
        t(3)
        o(d, 1)
        self.sanction = 0

    def ban(self):
        """
        Disables readings by the interrogator on a given time interval.
        """
        t = time.sleep

        # Increments the time sanction if it is possible.
        if self.sanction <= 30:
            t(self.sanction)
        else:
            t(30)
        self.sanction += 2


def led(q, evt):
    """
    Changes the RGB LED color according to the current status.
    :param q: queue containing the LED modes.
    :param evt: indicates that the event is enabled.
    """
    # RGB LED values
    r = 3
    g = 5
    b = 7

    # Locale methods conversion
    o = GPIO.output
    t = time.sleep

    sta = threading.local()
    sta.LED = False
    mode = 0  # READ mode.
    while evt.is_set():
        if q.empty() is False:
            try:
                mode = q.get()
            except queue.Empty():
                pass
        else:
            if mode == 0:  # READ mode -> Blue light
                o(b, 1)
                t(0.3)
                o(b, 0)
                t(5)
            elif mode == 1:  # OPEN mode -> Green light
                o(g, 1)
                t(0.1)
                o(g, 0)
                t(0.1)
            elif mode == 3:  # ADD mode -> Purple light
                o(r, 1)
                o(b, 1)
                t(0.1)
                o(r, 0)
                o(b, 0)
                t(0.1)
            else:
                # BAN mode -> Red light
                o(r, 1)
                t(0.1)
                o(r, 0)
                t(0.1)

    # Set RGB LED color to none.
    o(r, 0)
    o(b, 0)
    o(g, 0)


if __name__ == '__main__':
    reader = Reader()
    reader.init()
    while True:
        card = reader.read()
        print(card)
