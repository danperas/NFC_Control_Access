import sqlite3
import time


create_table_user = """ CREATE TABLE IF NOT EXISTS user (
                                uid int PRIMARY KEY,
                                name varchar(30) NOT NULL,
                                permission int NOT NULL
                                ); """

create_table_register = """ CREATE TABLE IF NOT EXISTS register (
                            date datetime PRIMARY KEY,
                            name varchar(30) NOT NULL,
                            door varchar(10) NOT NULL
                            ); """


class Database:
    def __init__(self, db=None):
        self.db = db
        self.con = None
        self.cur = None

    def conn(self):
        db = self.db
        try:
            self.con = sqlite3.connect(db)
            self.con.row_factory = sqlite3.Row
            self.cur = self.con.cursor()
        except sqlite3.Error as e:
            raise e
        else:
            self.cur.execute(create_table_user)
            self.cur.execute(create_table_register)

    def dis(self):
        try:
            self.con.close()
        except sqlite3.Error as e:
            raise e

    def chk(self, card):
        try:
            self.cur.execute("SELECT * FROM user WHERE uid = ?", (card,))
        except sqlite3.Error as e:
            self.con.rollback()
            raise e
        else:
            row = self.cur.fetchone()
            if row is None:
                raise ValueError
            else:
                return row[1]

    def add_r(self, name, door):
        try:
            self.cur.execute("INSERT INTO register VALUES(?, ?, ?)",
                             (name, time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()), door,))
        except sqlite3.Error as e:
            self.con.rollback()
            raise e
        else:
            self.con.commit()

