from flask import Flask, render_template, request, redirect, flash
from flask_sqlalchemy import SQLAlchemy
import json
import datetime

with open('config.json') as json_data_file:
    config_dict = json.load(json_data_file)

app = Flask(__name__, template_folder='templates')
app.config['SQLALCHEMY_DATABASE_URI'] = config_dict['database']['type'] + config_dict['database']['path']
db = SQLAlchemy(app)


class Usuarios(db.Model):
    __tablename__ = "usuarios"
    id = db.Column(db.Integer, primary_key=True)
    tarjeta = db.Column(db.String(50), primary_key=True)
    nombre = db.Column(db.String(30), unique=True, nullable=False)
    permiso = db.Column(db.Integer, nullable=False)


class Accesos(db.Model):
    __tablename__ = "accesos"
    id = db.Column(db.Integer, primary_key=True)
    nombre = db.Column(db.String(30), db.ForeignKey('usuarios.nombre'))
    fecha = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    puerta = db.Column(db.Integer, nullable=False)


db.create_all()

paginas = ['Inicio', 'Credenciales', 'Accesos', 'Reservas']


@app.route('/inicio', methods=['GET'])
@app.route('/', methods=['GET'])
def inicio():
    return render_template("inicio.html", pages=paginas, title="Inicio")


@app.route('/credenciales', methods=['GET'])
def credenciales():
    table_headers = []
    for k in Usuarios.__table__.columns.keys():
        table_headers.append(k.capitalize())
    if Usuarios.query.first() is None:
        return render_template("credenciales.html", notrows="No hay ningún usuario registrado.", table_title="Usuarios",
                               pages=paginas, title="Credenciales")
    else:
        return render_template("credenciales.html", table_content=Usuarios.query.order_by(Usuarios.id).all(),
                           table_headers=table_headers, table_title="Usuarios", pages=paginas,
                           title="Credenciales")


@app.route('/accesos', methods=['GET'])
def accesos():
    table_headers = []
    for k in Accesos.__table__.columns.keys():
        table_headers.append(k.capitalize())
    if Accesos.query.first() is None:
        return render_template("accesos.html", notrows="No hay ningún acceso realizado.", table_title="Accesos",
                               pages=paginas, title="Accesos")
    else:
        return render_template("accesos.html",
                               table_content=Accesos.query.order_by(Accesos.fecha.desc()).limit(50).all(),
                           table_headers=table_headers, table_title="Accesos", pages=paginas,
                           title="Accesos")


@app.route('/add_user', methods=['POST'])
def add_user():
    return redirect('/manager')


@app.route('/del_user', methods=['POST'])
def del_user():
    if request.method == 'POST':
        User.query.filter_by(uid=request.form['uid']).delete()
        db.session.commit()
        return redirect('/manager')




if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080)
